<?php

return [
    'environments' => [
        'production' => [
            'paths' => [
                '*' => [
                    'disallow' => [
                        '/admin',
                        '/api',
                    ],
                    'allow' => [],
                ],
            ],
            // 'sitemaps' => [
            //     '/sitemap.xml',
            // ],
        ],
    ],
];
