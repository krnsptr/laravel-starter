<?php

namespace Tests\Feature\Developer;

use App\Models\Developer;
use Tests\TestCase;

class DeveloperTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        config(['auth.providers.developer.enabled' => true]);
        $this->actingAs(Developer::firstOrFail(), 'developer');
    }

    public function test_developer_can_view_home()
    {
        $response = $this->get(route('developer.home'));
        $response->assertSuccessful();
    }

    public function test_developer_can_view_phpinfo()
    {
        $response = $this->get(route('developer.phpinfo'));
        $response->assertSuccessful();
    }
}
