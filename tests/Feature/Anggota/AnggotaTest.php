<?php

namespace Tests\Feature\Anggota;

use App\Models\Anggota;
use App\Models\AnggotaProfil;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AnggotaTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->actingAs($this->anggota(), 'anggota');
        \DB::beginTransaction();
    }

    public function tearDown(): void
    {
        \DB::rollback();

        parent::tearDown();
    }

    protected function anggota(): Anggota
    {
        return Anggota::firstOrFail();
    }

    protected function unverifiedAnggota(): Anggota
    {
        return Anggota::factory()->unvalidated()->unverified()->create();
    }

    public function test_anggota_can_view_home()
    {
        $response = $this->get(route('anggota.dasbor'));
        $response->assertSuccessful();
    }

    public function test_anggota_cannot_view_home_when_doesnt_have_profil()
    {
        $profil = $this->anggota()->profil;
        $profil->delete();

        $response = $this->get(route('anggota.dasbor'));
        $response->assertRedirect(route('anggota.profil'));
    }

    public function test_anggota_cannot_view_home_when_profil_not_up_to_date()
    {
        $profil = $this->anggota()->profil;
        $profil->updated_at = now()->subYear();
        $profil->save(['timestamps' => false]);

        $response = $this->get(route('anggota.dasbor'));
        $response->assertRedirect(route('anggota.profil'));
    }

    public function test_anggota_cannot_view_home_when_not_verified()
    {
        config(['auth.email.verification.enabled' => true]);

        $anggota = $this->unverifiedAnggota();
        $this->actingAs($anggota);

        $response = $this->get(route('anggota.dasbor'));
        $response->assertRedirect(route('anggota.akun.email'));
    }

    public function test_anggota_can_view_akun_page()
    {
        $response = $this->followingRedirects()->get(route('anggota.akun'));
        $response->assertSuccessful();
    }

    public function test_anggota_can_view_akun_email_page()
    {
        $response = $this->get(route('anggota.akun.email'));
        $response->assertSuccessful();
    }

    public function test_anggota_can_change_email_when_not_verified()
    {
        $anggota = $this->unverifiedAnggota();
        $this->actingAs($anggota);

        $response = $this->post(route('anggota.akun.email'), ['email' => "new.{$anggota->email}"]);
        $response->assertRedirect();
        $response->assertSessionHas('success');
    }

    public function test_anggota_cannot_change_email_when_verified()
    {
        $anggota = $this->anggota();

        $response = $this->post(route('anggota.akun.email'), ['email' => "new.{$anggota->email}"]);
        $response->assertRedirect();
        $response->assertSessionHasErrors('email');
    }

    public function test_anggota_can_view_akun_password_page()
    {
        $response = $this->get(route('anggota.akun.password'));
        $response->assertSuccessful();
    }

    protected function valuesForChangingPassword(): array
    {
        return [
            'current_password' => $this->anggota()->email,
            'new_password' => 'NEW-PASSWORD',
            'new_password_confirmation' => 'NEW-PASSWORD',
        ];
    }

    public function test_anggota_can_change_password()
    {
        $values = $this->valuesForChangingPassword();

        $response = $this->post(route('anggota.akun.password'), $values);
        $response->assertRedirect();
        $response->assertSessionHas('success');

        $this->assertTrue(\Hash::check($values['new_password'], $this->anggota()->password));
    }

    public function test_anggota_cannot_change_password_when_input_incomplete()
    {
        foreach (array_keys($this->valuesForChangingPassword()) as $key) {
            $values = array_merge($this->valuesForChangingPassword(), [$key => '']);
            $response = $this->post(route('anggota.akun.password'), $values);

            $response->assertRedirect();
            $response->assertSessionHasErrors(str_replace('_confirmation', '', $key));
        }
    }

    public function test_anggota_cannot_change_password_when_current_password_not_match()
    {
        $values = array_merge($this->valuesForChangingPassword(), ['current_password' => 'MISMATCH']);
        $response = $this->post(route('anggota.akun.password'), $values);

        $response->assertRedirect();
        $response->assertSessionHasErrors('current_password');
    }

    public function test_anggota_cannot_change_password_when_password_confirmation_not_match()
    {
        $values = array_merge($this->valuesForChangingPassword(), ['new_password_confirmation' => 'MISMATCH']);
        $response = $this->post(route('anggota.akun.password'), $values);

        $response->assertRedirect();
        $response->assertSessionHasErrors('new_password');
    }

    protected function valuesForUpdatingProfile(): array
    {
        $newUser = Anggota::factory()->make();
        $newProfile = AnggotaProfil::factory()->make();

        $anggota_fillables = ['nama'];
        $profil_fillables = $newProfile->getFillable();

        $values = [];

        foreach (array_merge($anggota_fillables, $profil_fillables) as $key) {
            $values[$key] = $newProfile->{$key} ?? $newUser->{$key};
        }

        return $values;
    }

    public function test_anggota_can_view_profil_page()
    {
        $response = $this->get(route('anggota.profil'));
        $response->assertSuccessful();
    }

    public function test_anggota_can_update_profil()
    {
        $anggota = $this->anggota();
        $profil = $anggota->profil;
        $values = $this->valuesForUpdatingProfile();

        $response = $this->post(route('anggota.profil'), $values);
        $response->assertRedirect();
        $response->assertSessionHas('success');

        $anggota->refresh();
        $profil->refresh();

        foreach ($values as $key => $value) {
            foreach ([$profil, $anggota] as $model) {
                if ($model->isFillable($key)) {
                    $this->assertEquals($value, $model->{$key});
                }
            }
        }
    }

    public function test_anggota_can_update_profil_foto()
    {
        $anggota = $this->anggota();
        $values = $this->valuesForUpdatingProfile();

        Storage::fake($anggota->fotoDisk());
        $values['foto'] = UploadedFile::fake()->image('foto1.jpg');

        $response = $this->postJson(route('anggota.profil'), $values);
        $response->assertSuccessful();

        $anggota->refresh();

        $this->assertIsString($anggota->foto);
        $this->assertNotEquals($anggota->fotoDefault(), $anggota->foto);
    }
}
