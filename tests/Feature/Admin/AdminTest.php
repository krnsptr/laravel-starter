<?php

namespace Tests\Feature\Admin;

use App\Models\Admin;
use Tests\TestCase;

class AdminTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->actingAs(Admin::firstOrFail(), 'admin');
    }

    public function test_admin_can_view_home()
    {
        $response = $this->get(route('admin.dasbor'));
        $response->assertSuccessful();
    }
}
