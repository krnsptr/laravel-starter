<?php

namespace Tests\Feature\Auth;

use App\Models\Anggota;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AnggotaRegisterTest extends TestCase
{
    public function test_anggota_can_view_registration_form()
    {
        $response = $this->get(route('anggota.register'));
        $response->assertSuccessful();
    }

    public function test_anggota_can_register_with_valid_data()
    {
        $newUser = Anggota::factory()->make();

        $response = $this->post(route('anggota.register'), [
            'nama' => $nama = $newUser->nama,
            'email' => $email = $newUser->email,
            'password' => $password = $newUser->email,
        ]);

        $response->assertRedirect();

        $anggota = Anggota::where('email', $email)->first();
        $this->assertNotNull($anggota);
        $this->assertEquals($nama, $anggota->nama);
        $this->assertTrue(Hash::check($password, $anggota->password));
        $this->assertAuthenticatedAs($anggota);
    }

    public function test_anggota_cannot_register_with_invalid_data()
    {
        $valid_data = [
            'nama' => 'VALID NAME',
            'email' => 'VALID@example.com',
            'password' => 'VALID_PASSWORD',
        ];

        $invalid_data = [
            'nama' => '  ', // empty
            'email' => 'INVALID', // invalid email address
            'password' => 'INVALID', // less than 8 characters
        ];

        foreach (array_keys($invalid_data) as $key) {
            $data = array_merge($valid_data, [$key => null]);

            if ($data['email'] === $valid_data['email']) {
                $data['email'] = Anggota::factory()->make()->email;
            }

            $this->post(route('anggota.register'), $data)->assertSessionHasErrors($key);
        }

        foreach ($invalid_data as $key => $invalid) {
            $data = array_merge($valid_data, [$key => $invalid]);

            if ($data['email'] === $valid_data['email']) {
                $data['email'] = Anggota::factory()->make()->email;
            }

            $response = $this->post(route('anggota.register'), $data);
            $response->assertRedirect();
            $response->assertSessionHasErrors($key);
        }
    }

    public function test_anggota_cannot_register_with_existing_email()
    {
        $existingUser = Anggota::factory()->create();

        $data = [
            'nama' => $existingUser->nama,
            'email' => $existingUser->email,
            'password' => 'password',
        ];

        $response = $this->post(route('anggota.register'), $data);
        $response->assertRedirect();
        $response->assertSessionHasErrors('email');
    }
}
