<?php

namespace Tests\Feature\Auth;

use App\Models\Anggota;
use App\Notifications\AnggotaVerifyEmailNotification;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class AnggotaEmailVerificationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Notification::fake();
    }

    private function unverifiedAnggota(): Anggota
    {
        return Anggota::factory()->unverified()->create();
    }

    public function test_anggota_can_verify_email()
    {
        $anggota = $this->unverifiedAnggota();

        $this->actingAs($anggota, 'anggota');

        $response = $this
            ->from(route('anggota.akun.email'))
            ->post(route('anggota.email.verification.resend'));

        $response->assertRedirect();
        $response->assertSessionHas('resent', true);

        $verification_url = null;

        Notification::assertSentTo(
            $anggota,
            AnggotaVerifyEmailNotification::class,
            function (AnggotaVerifyEmailNotification $notification) use ($anggota, &$verification_url) {
                $verification_url = $notification->verificationUrl;

                return $notification->anggota->email === $anggota->email;
            },
        );

        $response = $this->get($verification_url);
        $response->assertRedirect();
        $response->assertSessionHas('verified');

        $anggota->refresh();
        $this->assertNotNull($anggota->email_verified_at);
    }
}
