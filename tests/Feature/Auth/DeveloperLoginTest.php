<?php

namespace Tests\Feature\Auth;

use App\Models\Developer;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeveloperLoginTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        config(['auth.providers.developer.enabled' => true]);
        config(['auth.providers.developer.username' => 'test.dev@example.com']);
        config(['auth.providers.developer.password' => 'test.dev@example.com']);
    }

    private function existingDeveloper(): Developer
    {
        return Developer::firstOrFail();
    }

    public function test_developer_can_view_login_form()
    {
        $response = $this->get(route('developer.login'));
        $response->assertSuccessful();
    }

    public function test_developer_cannot_view_login_form_when_authenticated()
    {
        $response = $this
            ->actingAs($this->existingDeveloper(), 'developer')
            ->get(route('developer.login'));

        $response->assertRedirect();
    }

    public function test_developer_can_login_with_correct_credentials()
    {
        $developer = $this->existingDeveloper();

        $response = $this->post(route('developer.login'), [
            'username' => config('auth.providers.developer.username'),
            'password' => config('auth.providers.developer.password'),
        ]);

        $response->assertRedirect(route('developer.home'));
        $this->assertAuthenticatedAs($developer, 'developer');
    }

    public function test_developer_cannot_login_with_incorrect_username()
    {
        $response = $this
            ->from(route('developer.login'))
            ->post(route('developer.login'), [
                'username' => 'NOT_EXIST@example.com',
                'password' => config('auth.providers.developer.password'),
            ]);

        $response->assertRedirect(route('developer.login'));
        $response->assertSessionHasErrors('username');
        $this->assertTrue(session()->hasOldInput('username'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function test_developer_cannot_login_with_incorrect_password()
    {
        $response = $this
            ->from(route('developer.login'))
            ->post(route('developer.login'), [
                'username' => config('auth.providers.developer.username'),
                'password' => 'INCORRECT_PASSWORD',
            ]);

        $response->assertRedirect(route('developer.login'));
        $response->assertSessionHasErrors('username');
        $this->assertTrue(session()->hasOldInput('username'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function test_developer_can_view_logout_form()
    {
        $response = $this
            ->actingAs($this->existingDeveloper(), 'developer')
            ->get(route('developer.logout'));

        $response->assertSuccessful();
    }

    public function test_developer_can_logout_when_authenticated()
    {
        $this->test_developer_can_login_with_correct_credentials();

        $response = $this
            ->from(route('developer.home'))
            ->post(route('developer.logout'));

        $response->assertRedirect();
        $this->assertGuest();
    }

    public function test_developer_login_throttled()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->post(route('developer.login'), [
                'username' => 'NOT_EXIST@example.com',
                'password' => 'INCORRECT_PASSWORD',
            ]);
        }

        $response = $this->postJson(route('developer.login'), [
            'username' => 'NOT_EXIST@example.com',
            'password' => 'INCORRECT_PASSWORD',
        ]);

        $response->assertStatus(Response::HTTP_TOO_MANY_REQUESTS);
    }

    public function test_developer_cannot_enter_when_feature_is_disabled()
    {
        config(['auth.providers.developer.enabled' => false]);

        $response = $this->get(route('developer.login'));
        $response->assertNotFound();

        $response = $this->get(route('developer.home'));
        $response->assertNotFound();
    }
}
