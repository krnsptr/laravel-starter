<?php

namespace Tests\Feature\Auth;

use App\Models\Anggota;
use App\Notifications\AnggotaForgotPasswordNotification;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Notification;
use Tests\TestCase;

class AnggotaForgotPasswordTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Notification::fake();
    }

    private function existingAnggota(): Anggota
    {
        return Anggota::factory()->create();
    }

    public function test_anggota_can_view_forgot_password_form()
    {
        $response = $this->get(route('anggota.password.forgot'));
        $response->assertSuccessful();
    }

    public function test_anggota_can_reset_password_with_correct_email()
    {
        $anggota = $this->existingAnggota();

        $response = $this
            ->from(route('anggota.password.forgot'))
            ->post(route('anggota.password.forgot'), [
                'email' => $anggota->email,
            ]);

        $response->assertRedirect(route('anggota.password.forgot'));
        $response->assertSessionHasNoErrors();

        $token_hash = \DB::table(config('auth.passwords.anggota.table'))
            ->where('email', $anggota->email)
            ->latest()
            ->value('token');

        $token = null;

        $this->assertNotNull($token_hash);

        Notification::assertSentTo(
            $anggota,
            AnggotaForgotPasswordNotification::class,
            function (AnggotaForgotPasswordNotification $notification) use ($token_hash, &$token) {
                $token = $notification->token;

                return Hash::check($token, $token_hash);
            },
        );

        $response = $this->get(route('anggota.password.reset', [
            'email' => $anggota->email,
            'token' => $token,
        ]));

        $response->assertSuccessful();

        $response = $this
            ->post(route('anggota.password.reset'), [
                'token' => $token,
                'email' => $anggota->email,
                'password' => 'NEW_PASSWORD',
                'password_confirmation' => 'NEW_PASSWORD',
            ]);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $anggota = Anggota::where('email', $anggota->email)->first();
        $this->assertNotNull($anggota);
        $this->assertTrue(Hash::check('NEW_PASSWORD', $anggota->password));
    }

    public function test_anggota_cannot_reset_password_with_incorrect_email()
    {
        $response = $this->post(route('anggota.password.forgot'), [
            'email' => 'NOT_EXIST@example.com',
        ]);

        $response->assertSessionHasErrors('email');
    }

    public function test_anggota_reset_password_throttled()
    {
        $anggota = $this->existingAnggota();

        for ($i = 0; $i < 10; $i++) {
            $this->post(route('anggota.password.forgot'), [
                'email' => $anggota->email,
            ]);
        }

        $response = $this->postJson(route('anggota.password.forgot'), [
            'email' => $anggota->email,
        ]);

        $response->assertStatus(Response::HTTP_TOO_MANY_REQUESTS);
    }
}
