<?php

namespace Tests\Feature\Auth;

use App\Models\Anggota;
use Illuminate\Http\Response;
use Tests\TestCase;

class AnggotaLoginTest extends TestCase
{
    private function existingAnggota(): Anggota
    {
        return Anggota::factory()->create();
    }

    public function test_anggota_can_view_login_form()
    {
        $response = $this->get(route('anggota.login'));
        $response->assertSuccessful();
    }

    public function test_anggota_cannot_view_login_form_when_authenticated()
    {
        $response = $this->actingAs($this->existingAnggota())->get(route('anggota.login'));
        $response->assertRedirect();
    }

    public function test_anggota_can_login_with_correct_credentials()
    {
        $anggota = $this->existingAnggota();

        $response = $this->post(route('anggota.login'), [
            'email' => $anggota->email,
            'password' => 'password',
        ]);

        $response->assertRedirect(route('anggota.dasbor'));
        $this->assertAuthenticatedAs($anggota);
    }

    public function test_anggota_cannot_login_with_incorrect_email()
    {
        $response = $this
            ->from(route('anggota.login'))
            ->post(route('anggota.login'), [
                'email' => 'NOT_EXIST@example.com',
                'password' => 'password',
            ]);

        $response->assertRedirect(route('anggota.login'));
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function test_anggota_cannot_login_with_incorrect_password()
    {
        $anggota = $this->existingAnggota();

        $response = $this
            ->from(route('anggota.login'))
            ->post(route('anggota.login'), [
                'email' => $anggota->email,
                'password' => 'INCORRECT_PASSWORD',
            ]);

        $response->assertRedirect(route('anggota.login'));
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function test_anggota_can_view_logout_form()
    {
        $response = $this->actingAs($this->existingAnggota(), 'anggota')->get(route('anggota.logout'));
        $response->assertSuccessful();
    }

    public function test_anggota_cannot_view_logout_form_when_authenticated()
    {
        $response = $this->get(route('anggota.logout'));
        $response->assertRedirect();
    }

    public function test_anggota_can_logout_when_authenticated()
    {
        $response = $this
            ->actingAs($this->existingAnggota(), 'anggota')
            ->from(route('anggota.dasbor'))
            ->post(route('anggota.logout'));

        $response->assertRedirect();
        $this->assertGuest('anggota');
    }

    public function test_anggota_login_throttled()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->post(route('anggota.login'), [
                'email' => 'NOT_EXIST@example.com',
                'password' => 'INCORRECT_PASSWORD',
            ]);
        }

        $response = $this->postJson(route('anggota.login'), [
            'email' => 'NOT_EXIST@example.com',
            'password' => 'INCORRECT_PASSWORD',
        ]);

        $response->assertStatus(Response::HTTP_TOO_MANY_REQUESTS);
    }
}
