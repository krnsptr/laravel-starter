<?php

namespace Tests\Feature\Auth;

use App\Models\Admin;
use Illuminate\Http\Response;
use Tests\TestCase;

class AdminLoginTest extends TestCase
{
    private function existingAdmin(): Admin
    {
        $email = 'TEST.ADMIN@example.com';

        $admin = Admin::where('email', $email)->firstOrNew();

        if (!$admin->exists) {
            $admin->nama = $email;
            $admin->email = $email;
            $admin->password = bcrypt('password');
            $admin->save();
        }

        return $admin;
    }

    public function test_admin_can_view_login_form()
    {
        $response = $this->get(route('admin.login'));
        $response->assertSuccessful();
    }

    public function test_admin_cannot_view_login_form_when_authenticated()
    {
        $response = $this->actingAs($this->existingAdmin())->get(route('admin.login'));
        $response->assertRedirect();
    }

    public function test_admin_can_login_with_correct_credentials()
    {
        $admin = $this->existingAdmin();

        $response = $this->post(route('admin.login'), [
            'email' => $admin->email,
            'password' => 'password',
        ]);

        $response->assertRedirect();
        $this->assertAuthenticatedAs($admin);
    }

    public function test_admin_cannot_login_with_incorrect_email()
    {
        $response = $this
            ->from(route('admin.login'))
            ->post(route('admin.login'), [
                'email' => 'NOT_EXIST@example.com',
                'password' => 'password',
            ]);

        $response->assertRedirect(route('admin.login'));
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function test_admin_cannot_login_with_incorrect_password()
    {
        $admin = $this->existingAdmin();

        $response = $this
            ->from(route('admin.login'))
            ->post(route('admin.login'), [
                'email' => $admin->email,
                'password' => 'INCORRECT_PASSWORD',
            ]);

        $response->assertRedirect(route('admin.login'));
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function test_admin_can_view_logout_form()
    {
        $response = $this->actingAs($this->existingAdmin(), 'admin')->get(route('admin.logout'));
        $response->assertSuccessful();
    }

    public function test_admin_cannot_view_logout_form_when_authenticated()
    {
        $response = $this->get(route('admin.logout'));
        $response->assertRedirect();
    }

    public function test_admin_can_logout_when_authenticated()
    {
        $response = $this
            ->actingAs($this->existingAdmin(), 'admin')
            ->from(route('admin.dasbor'))
            ->post(route('admin.logout'));

        $response->assertRedirect();
        $this->assertGuest('admin');
    }

    public function test_admin_login_throttled()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->post(route('admin.login'), [
                'email' => 'NOT_EXIST@example.com',
                'password' => 'INCORRECT_PASSWORD',
            ]);
        }

        $response = $this->postJson(route('admin.login'), [
            'email' => 'NOT_EXIST@example.com',
            'password' => 'INCORRECT_PASSWORD',
        ]);

        $response->assertStatus(Response::HTTP_TOO_MANY_REQUESTS);
    }
}
