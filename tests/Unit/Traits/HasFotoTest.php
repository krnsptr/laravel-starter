<?php

namespace Tests\Unit\Traits;

use App\Traits\HasFoto;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class HasFotoTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $model = $this->newModel();
        $disk = $model->fotoDisk();
        Storage::fake($disk);

        $default_foto = $model::fotoDefault();
        $default_foto_path = $model->fotoPath();

        if (!$model->fotoStorage()->exists($default_foto_path)) {
            $model->fotoStorage()->putFileAs(
                $model->fotoDir(),
                UploadedFile::fake()->image($default_foto),
                $default_foto,
            );
        }
    }

    /**
     * Instantiate new model implementing the trait
     *
     * @return Model&HasFoto
     */
    private function newModel()
    {
        return new class () extends Model {
            use HasFoto;

            protected $table = '_test_model_has_foto';

            public $timestamps = false;

            protected function performInsert(Builder $query)
            {
                $this->exists = true;

                return true;
            }

            protected function performUpdate(Builder $query)
            {
                return true;
            }

            protected function performDeleteOnModel()
            {
                $this->exists = false;
            }
        };
    }

    public function test_default_foto_url()
    {
        $model = $this->newModel();
        $filename = $model->fotoDefault();

        $this->assertStringEndsWith($filename, $model->getFotoUrlAttribute());
    }

    public function test_foto_url()
    {
        $model = $this->newModel();
        $filename = 'foto.jpg';
        $model->foto = $filename;

        $this->assertStringEndsWith($filename, $model->getFotoUrlAttribute());
    }

    public function test_set_foto()
    {
        $model = $this->newModel();
        $model->setFoto(UploadedFile::fake()->image('foto.jpg'));
        $model->save();

        // Ensure foto is set
        $this->assertIsString($model->foto);

        // Ensure foto is not default foto
        $this->assertNotEquals($model::fotoDefault(), $model->foto);

        // Ensure foto file exists
        $model->fotoStorage()->assertExists($model->fotoPath());

        // Ensure default foto is not deleted
        $model->fotoStorage()->assertExists($this->newModel()->fotoPath());
    }

    public function test_unset_foto()
    {
        $model = $this->newModel();
        $model->setFoto(UploadedFile::fake()->image('foto1.jpg'));
        $model->save();

        $foto_path = $model->fotoPath();

        // Ensure foto file exists
        $model->fotoStorage()->assertExists($foto_path);

        $model->foto = null;
        $model->save();

        // Ensure foto file is deleted
        $model->fotoStorage()->assertMissing($foto_path);
    }

    public function test_update_foto()
    {
        $model = $this->newModel();
        $model->setFoto(UploadedFile::fake()->image('foto1.jpg'));
        $model->save();

        $old_foto_path = $model->fotoPath();

        $model->setFoto(UploadedFile::fake()->image('foto2.jpg'));
        $model->save();

        $new_foto_path = $model->fotoPath();

        // Ensure new foto file exists
        $model->fotoStorage()->assertExists($new_foto_path);

        // Ensure old foto file is deleted
        $model->fotoStorage()->assertMissing($old_foto_path);
    }

    public function test_delete_model()
    {
        $model = $this->newModel();
        $model->setFoto(UploadedFile::fake()->image('foto1.jpg'));
        $model->save();
        $model->delete();

        $foto_path = $model->fotoPath();

        // Ensure foto file is deleted
        $model->fotoStorage()->assertMissing($foto_path);
    }
}
