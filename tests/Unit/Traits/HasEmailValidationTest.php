<?php

namespace Tests\Unit\Traits;

use App\Traits\HasEmailValidation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class HasEmailValidationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        config(['auth.email.validation.enabled' => true]);
    }

    /**
     * Instantiate new model implementing the trait
     *
     * @return Model&HasEmailValidation
     */
    private function newModel()
    {
        return new class () extends Model {
            use HasEmailValidation;

            protected function performInsert(Builder $query)
            {
                $this->exists = true;

                return true;
            }

            protected function performUpdate(Builder $query)
            {
                return true;
            }

            protected function performDeleteOnModel()
            {
                $this->exists = false;
            }

            public function setOriginalEmail($email)
            {
                $original = $this->original ?? [];
                $original['email'] = $email;
                $this->original = $original;
            }
        };
    }

    public function test_validation_disabled()
    {
        config(['auth.email.validation.enabled' => false]);

        $model = $this->newModel();
        $model->email = 'support@google.com';
        $model->save();

        $this->assertNull($model->email_validated_at);

        $model = $this->newModel();
        $model->email = 'mail@example.invalid';
        $model->save();

        $this->assertNull($model->email_validated_at);
    }

    public function test_set_invalid_email()
    {
        $this->expectException(ValidationException::class);

        $model = $this->newModel();
        $model->email = 'mail@example.invalid';
        $model->save();

        $this->assertNull($model->email_validated_at);
    }

    public function test_set_unchanged_email()
    {
        $email_validated_at_string = '2021-01-01';

        $model = $this->newModel();
        $model->setOriginalEmail('support@google.com');
        $model->email_validated_at = $email_validated_at_string;
        $email_validated_at_before = $model->email_validated_at;

        $model->email = 'support@google.com';
        $model->save();
        $email_validated_at_after = $model->email_validated_at;

        $this->assertEquals($email_validated_at_before, $email_validated_at_after);
    }

    public function test_unset_email()
    {
        $model = $this->newModel();
        $model->setOriginalEmail('support@google.com');
        $model->email_validated_at = '2021-01-01';

        $model->email = null;
        $model->save();

        $this->assertNull($model->email_validated_at);
    }

    public function test_skip_email_validation()
    {
        $model = $this->newModel();
        $model->email = 'mail@example.invalid';
        $model->saveWithoutEmailValidation();

        $this->assertNull($model->email_validated_at);
    }
}
