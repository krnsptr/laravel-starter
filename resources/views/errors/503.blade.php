@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Service Unavailable'))

@section('image')
  <div style="background-image: url({{ asset('/errors/503.svg') }});"
    class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
  </div>
@endsection

@section('message')
  @if (isset($exception) && !empty($exception->getMessage()))
    {{ __($exception->getMessage()) }}
  @else
    {{ __('Sorry, we are doing some maintenance. Please check back soon.') }}
  @endif
@endsection
