@extends('errors.illustrated-layout')

@section('code', '429')
@section('title', __('Too Many Requests'))

@section('image')
  <div style="background-image: url({{ asset('/errors/403.svg') }});"
    class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
  </div>
@endsection

@section('message')
  Kami suka semangat kamu, tapi harap tunggu sebentar ya.
  <ul style="margin: 0;">
    <li>Email mungkin <strong>tertunda</strong> 2-5 menit.</li>
    <li>
      Pastikan alamat email <strong>benar</strong>.
      <span style="font-family: monospace, monospace;">{{ $email ?? '' }}</span>
    </li>
    <li>Pastikan kotak masuk <strong>tidak penuh</strong>.</li>
    <li>Cek semua label <strong>(Sosial, Promosi, dll)</strong> dan <strong>spam</strong>.</li>
    <li>Silakan <strong>coba lagi</strong> dalam beberapa menit atau hubungi kami.</li>
  </ul>
  <br><br>
@endsection
