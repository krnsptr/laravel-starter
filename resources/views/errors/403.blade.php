@extends('errors::illustrated-layout')

@section('code', '403')
@section('title', __('Forbidden'))

@section('image')
  <div style="background-image: url({{ asset('/errors/403.svg') }});"
    class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
  </div>
@endsection

@section('message')
  @if (isset($exception) && !empty($exception->getMessage()))
    {{ __($exception->getMessage()) }}
  @else
    {{ __('Sorry, you are forbidden from accessing this page.') }}
  @endif
@endsection
