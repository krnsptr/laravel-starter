<script>
  if (window.jQuery) {
    // Add csrf token to ajax post requests
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      if (originalOptions.type === 'POST' || options.type === 'POST') {
        options.beforeSend = function(xhr) {
          xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        };
      }
    });

    // Show/hide passwords using toggle
    $('.password-toggle').click(function() {
      inputGroup = $(this).closest('.input-group');
      input = $(inputGroup).find('input');
      input.attr('type', (input.attr('type') === 'text') ? 'password' : 'text');
      input.next().find('i').toggleClass('d-none');
    });
  }
</script>
