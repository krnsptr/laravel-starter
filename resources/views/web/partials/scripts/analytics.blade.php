@if (App::environment('production') && !Route::isProtected() && $ga_id = config('services.ga.tracking_id'))
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id={{ $ga_id }}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', @json($ga_id));
  </script>
@endif
