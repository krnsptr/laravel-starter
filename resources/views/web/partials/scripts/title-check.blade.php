@env('local')
  <script>
    if (
      !document.title || (
        document.title === @json(config('seotools.meta.defaults.title'))
        && window.location.pathname !== '/'
      )) {
      alert('This page has no specific title. <title>' + document.title + '</title>');
    }
  </script>
@endenv
