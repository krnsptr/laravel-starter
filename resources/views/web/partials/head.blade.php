<meta name="csrf-token" content="{{ csrf_token() }}">

{!! SEO::generate() !!}

@include('web.partials.scripts.telemetry')
@include('web.partials.scripts.analytics')
@include('web.partials.scripts.error-alert')
@include('web.partials.scripts.title-check')

@stack('head')

@stack('head-css')
@stack('head-js')
