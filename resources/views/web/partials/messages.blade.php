@if ($info = $info ?? (session('info') ?? session('status')))
  <div class="alert alert-info alert-dismissible fade show">
    {!! $info !!}
    <button type="button" class="close btn-close"
      data-dismiss="alert" data-bs-dismiss="alert" aria-label="Close">
      <span aria-hidden="true" class="visually-hidden">&times;</span>
    </button>
  </div>
@endif

@if ($success = $success ?? session('success'))
  <div class="alert alert-success alert-dismissible fade show">
    {!! $success !!}
    <button type="button" class="close btn-close"
      data-dismiss="alert" data-bs-dismiss="alert" aria-label="Close">
      <span aria-hidden="true" class="visually-hidden">&times;</span>
    </button>
  </div>
@endif

@if ($warning = $warning ?? session('warning'))
  <div class="alert alert-warning alert-dismissible fade show">
    {!! $warning !!}
    <button type="button" class="close btn-close"
      data-dismiss="alert" data-bs-dismiss="alert" aria-label="Close">
      <span aria-hidden="true" class="visually-hidden">&times;</span>
    </button>
  </div>
@endif

@if ($error = $error ?? session('error'))
  <div class="alert alert-danger alert-dismissible fade show">
    {!! $error !!}
    <button type="button" class="close btn-close"
      data-dismiss="alert" data-bs-dismiss="alert" aria-label="Close">
      <span aria-hidden="true" class="visually-hidden">&times;</span>
    </button>
  </div>
@endif

@if ($errors->any())
  <div class="alert alert-danger alert-dismissible fade show">
    @foreach ($errors->all() as $error)
      {{ $error }}<br>
    @endforeach
    <button type="button" class="close btn-close"
      data-dismiss="alert" data-bs-dismiss="alert" aria-label="Close">
      <span aria-hidden="true" class="visually-hidden">&times;</span>
    </button>
  </div>
@endif
