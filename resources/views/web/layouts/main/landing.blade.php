@extends('web.layouts.main.dashboard')

@section('page')
  @include('web.layouts.main.inc.header-guest')
  @yield ('content')
@endsection
