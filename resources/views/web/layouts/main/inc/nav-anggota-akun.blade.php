<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link @routeis('anggota.akun.email') active @endrouteis"
      href="{{ route('anggota.akun.email') }}">
      Email
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link @routeis('anggota.akun.password') active @endrouteis"
      href="{{ route('anggota.akun.password') }}">
      Password
    </a>
  </li>
</ul>

