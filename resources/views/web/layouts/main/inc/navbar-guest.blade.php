<nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
  <div class="container px-4">
    <!-- Brand -->
    <a class="navbar-brand" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/ahawebs-logo.png') }}" />
    </a>

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse"
      data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapse header -->
    <div class="collapse navbar-collapse" id="navbar-collapse-main">
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/ahawebs-logo.png') }}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
              aria-label="Toggle sidenav">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navbar items -->
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a href="{{ route('anggota.login') }}"
            class="nav-link nav-link-icon @routeis('anggota.login') active @endrouteis">
            <i class="fas fa-sign-in-alt fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Masuk</span>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('anggota.register') }}"
            class="nav-link nav-link-icon @routeis('anggota.register') active @endrouteis">
            <i class="fas fa-user-plus fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Daftar</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
