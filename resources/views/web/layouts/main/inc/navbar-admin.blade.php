<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
  <div class="container-fluid">
    <div></div>

    <!-- Navbar items -->
    <ul class="navbar-nav align-items-center d-none d-md-flex">
      <li class="nav-item dropdown">
        <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <div class="media align-items-center">
            <span class="avatar avatar-sm rounded-circle">
              <img alt="{{ admin()->nama }}" src="{{ admin()->foto_url }}" class="p-1 bg-secondary">
            </span>
            <div class="media-body ml-2 d-none d-lg-block">
              <span class="mb-0 text-sm font-weight-bold">
                {{ admin()->nama }}
              </span>
            </div>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
          <a href="{{ route('admin.logout') }}" class="dropdown-item"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
            <span>Keluar</span>
          </a>
        </div>
      </li>
    </ul>

    @push('foot')

    <form method="post" action="{{ route('admin.logout') }}" id="logout-form" style="display: none;">
      @csrf
    </form>

    @endpush

  </div>
</nav>
