@if (Route::is('admin.*'))
  <div class="header bg-gradient-primary py-6 py-lg-7 mb-3 mb-lg-0">
  </div>
@else
  <div class="header bg-gradient-primary py-7 pt-lg-8 mb-6 mb-lg-0">
  </div>
@endif
