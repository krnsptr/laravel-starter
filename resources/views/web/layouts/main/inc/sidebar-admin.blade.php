<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
  <div class="container-fluid">

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse"
      data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Brand -->
    <a class="navbar-brand py-0" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/ahawebs-logo.png') }}" class="navbar-brand-img"
        alt="{{ config('app.name') }}">
    </a>

    <!-- User -->
    <ul class="nav align-items-center d-md-none">
      <li class="nav-item dropdown">
        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <div class="media align-items-center">
            <span class="avatar avatar-sm rounded-circle">
              <img alt="{{ auth('admin')->user()->nama }}" src="{{ admin()->foto_url }}"
                class="p-1 bg-secondary">
            </span>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
          <a href="{{ route('admin.logout') }}" class="dropdown-item"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
            <span>Keluar</span>
          </a>
        </div>
      </li>
    </ul>

    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/ahawebs-logo.png') }}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
              aria-label="Toggle sidenav">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navigation -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link @routeis('admin.dasbor') active @endrouteis" href="{{ route('admin.dasbor') }}">
            <i class="fas fa-columns fa-fw mr-1"></i>
            Dasbor
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
