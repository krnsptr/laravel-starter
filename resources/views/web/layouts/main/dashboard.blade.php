<!doctype html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  @include('web.partials.head')

  <link href="{{ asset('assets/img/brand/ahawebs-icon.png') }}" rel="icon" type="image/png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" crossorigin>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.0/css/all.css" crossorigin>

  <link rel="stylesheet" href="{{ asset('skins/argon/vendor/nucleo/css/nucleo.css') }}">
  <link rel="stylesheet" href="{{ asset('skins/argon/css/argon-dashboard.css?v=1.1.0') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/common.css?v=1') }}">
  <link rel="stylesheet" href="{{ asset('skins/argon/css/custom.css?v=2') }}">
</head>

<body class="{{ $body_class ?? '' }}">
  @auth('admin')
    @if (Route::is('admin.*'))
      @include('web.layouts.main.inc.sidebar-admin')
    @endif
  @endauth

  <div class="main-content d-flex flex-column" style="min-height: 86vh;">
    @auth('anggota')
      @if (!Route::is('admin.*'))
        @include('web.layouts.main.inc.navbar-anggota')
      @endif
    @endauth

    @auth('admin')
      @if (Route::is('admin.*'))
        @include('web.layouts.main.inc.navbar-admin')
      @else
        @include('web.layouts.main.inc.navbar-admin-landing')
      @endif
    @endauth

    @guest
      @include('web.layouts.main.inc.navbar-guest')
    @endguest

    @section('page')
      @auth
        @include('web.layouts.main.inc.header-auth')

        @if (Route::is('anggota.*', 'admin.*'))
          <div class="mt--8 mt-md--7" id="page-container">
            @yield ('content')
          </div>
        @else
          <div class="mt--3 mt-md-5" id="page-container">
            @yield ('content')
          </div>
        @endif
      @endauth

      @guest
        @include('web.layouts.main.inc.header-guest')
        @yield ('content')
      @endguest
    @show

    @include('web.layouts.main.inc.footer')
  </div>

  <script src="{{ asset('skins/argon/vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('skins/argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('skins/argon/js/argon-dashboard.js?v=1.1.0') }}"></script>

  @include('web.partials.foot')
</body>

</html>
