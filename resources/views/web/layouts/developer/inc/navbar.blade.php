<nav class="navbar navbar-light bg-white">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('developer.home') }}">
      <i class="iconify-inline fs-4 me-1" data-icon="fa-solid:code"></i>
      Developer Zone
    </a>

    @auth ('developer')
      <a class="float-right" href="{{ route('developer.logout') }}">
        Logout
      </a>
    @endauth
  </div>
</nav>
