<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link rel="stylesheet"
      href="https://unpkg.com/bootswatch@5.1.3/dist/darkly/bootstrap.min.css"
      integrity="sha384-CXyahs/yRWQp3Mi4PONePAtgkMCCs+Uy3SnqfSU5AG3XspRG94Oqq9zcDJY/3SAR"
      crossorigin="anonymous">

    <link rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"
      crossorigin="anonymous">

    <style>
      body {
        font-family: 'Nunito', sans-serif;
      }
    </style>

    <script src="https://code.iconify.design/2/2.1.0/iconify.min.js"></script>
  </head>
  <body>
    @include('web.layouts.developer.inc.navbar')

    @yield('content')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
      crossorigin="anonymous"></script>
  </body>
</html>
