@extends('web.layouts.developer.layout')

@php
  SEO::setTitle("Developer Zone");
@endphp

@section('content')

  <div class="container p-3">
    <div class="row">
      <div class="col-xl-8 col-lg-10 mx-auto">
        <div class="row my-4">
          <div class="col-12 text-center">
            Welcome,
            {{ developer()->username }}.
          </div>
        </div>

        <style>
          .main-menu .menu-icon {
            font-size: 3em;
            margin-bottom: 0.5em;
          }

        </style>

        <div class="row text-center main-menu" style="font-size: large;">
          <div class="col-6 mx-auto my-2">
            <a href="{{ route('developer.phpinfo') }}" class="card" target="_blank">
              <div class="card-body">
                <i class="iconify menu-icon mb-2" data-icon="fa-brands:php"></i>
                <br>
                PHP Info
              </div>
            </a>
          </div>

          @if (Route::has('telescope'))
             <div class="col-6 mx-auto my-2">
               <a href="{{ route('telescope') }}" class="card" target="_blank">
                 <div class="card-body">
                   <i class="iconify menu-icon mb-2" data-icon="ion:telescope"></i>
                   <br>
                   Telescope
                 </div>
               </a>
             </div>
          @endif

          @if (Route::has('pretty-routes.show'))
             <div class="col-6 mx-auto my-2">
               <a href="{{ route('pretty-routes.show') }}" class="card" target="_blank">
                 <div class="card-body">
                   <i class="iconify menu-icon mb-2" data-icon="fa-solid:map-signs"></i>
                   <br>
                   Routes
                 </div>
               </a>
             </div>
          @endif

          <div class="col-6 mx-auto my-2">
            <a href="{{ url(config('web-tinker.path')) }}" class="card" target="_blank">
              <div class="card-body">
                <i class="iconify menu-icon mb-2" data-icon="fa-solid:terminal"></i>
                <br>
                Tinker
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
