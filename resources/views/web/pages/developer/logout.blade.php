@extends('web.layouts.developer.layout')

@php
  SEO::setTitle("Developer Zone | Logout");
@endphp

@section('content')
  <div class="container p-3" style="height: 80vh;">
    @messages

    <div class="row h-100">
      <div class="col-md-8 col-lg-6 col-xl-4 my-3 my-md-auto mx-auto">
        <div class="card text-center">
          <div class="card-header p-3">
            <h1 class="h3">Logout</h1>
          </div>
          <div class="card body p-3 text-start">
            <form method="post" action="{{ url()->current() }}" autocomplete="off">
              @csrf

              <div class="d-grid gap-2">
                <button type="submit" class="btn btn-block btn-primary">
                  Logout
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
