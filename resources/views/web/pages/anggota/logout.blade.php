@extends('web.layouts.main.auth')

@php SEO::setTitle("Logout") @endphp

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center mb-0">Keluar</h1>
        </div>

        <div class="px-3 pt-3">
          @messages
        </div>

        <div class="card-body px-lg-5 pt-0">
          <form method="post" action="{{ url()->current() }}" autocomplete="off">
            @csrf

            <div class="d-grid gap-2">
              <button type="submit" class="btn btn-block btn-primary">
                Keluar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
