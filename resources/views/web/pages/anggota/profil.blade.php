@extends('web.layouts.main.dashboard')

@php SEO::setTitle("Edit Profil") @endphp

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 mx-auto">
        <div class="card shadow h-100">
          <div class="card-header d-md-flex">
            <h2 class="mb-0">Edit Profil</h2>
          </div>
          <div class="card-body" style="min-height: 52vh;">
            @messages

            <form method="post" action="{{ url()->current() }}" autocomplete="off" id="profil-form">
              @csrf

              <div class="row">
                <div class="col-md-10 col-lg-6 mx-auto">
                  <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">
                      Nama lengkap
                    </label>
                    <div class="col">
                      <input type="text" name="nama" id="nama" class="form-control"
                        value="{{ $anggota->nama }}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">
                      Jenis kelamin
                    </label>
                    <div class="col">
                      <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                        <option value=""></option>
                        @foreach (App\Enums\JenisKelamin::getInstances() as $jenis_kelamin)
                          <option value="{{ $jenis_kelamin->value }}" @selected($profil->jenis_kelamin === $jenis_kelamin->value)>
                            {{ $jenis_kelamin->description }}
                          </option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">
                      Tanggal lahir
                    </label>
                    <div class="col">
                      <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control"
                        placeholder="HH-BB-TTTT" value="{{ format_date_php($profil->tanggal_lahir) }}"
                        required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">
                      Nomor ponsel
                    </label>
                    <div class="col">
                      <input type="tel" name="ponsel" id="ponsel" class="form-control"
                        value="{{ $profil->ponsel }}" required>
                    </div>
                  </div>
                </div>

                <div class="col-md-10 col-lg-6 mx-auto">
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                      Foto profil
                    </label>
                    <div class="col-md-4 col-xl-5 my-3 text-center">
                      <img src="{{ $anggota->foto_url }}" id="foto-img" class="rounded"
                        style="max-height: 105px;">
                      <br>
                      <button type="button" class="btn btn-primary mt-2" data-toggle="modal"
                        data-target="#foto-modal">
                        <span class="mx-3">Ubah</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <hr class="my-3">

              <div class="row">
                <div class="col-md-4 col-lg-3 mx-auto">
                  <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="foto-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Ubah Foto Profil</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <label>Foto profil</label><br>
          <input type="file" accept="image/*" id="foto-input">
          <br>
          <div id="foto-croppie" class="mx-auto" style="height: 400px; display: none;"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            Batal
          </button>
          <button type="button" class="btn btn-primary" id="foto-save-button" disabled>
            Simpan
          </button>
        </div>
      </div>
    </div>
  @endsection

  @push('css')
    <link rel="stylesheet" href="https://unpkg.com/flatpickr@4.6.9/dist/flatpickr.min.css" crossorigin>
    <link rel="stylesheet" href="https://unpkg.com/croppie@2.6.5/croppie.css" crossorigin>
  @endpush


  @push('js')
    <script src="https://unpkg.com/flatpickr@4.6.9/dist/flatpickr.min.js" crossorigin></script>
    <script src="https://unpkg.com/imask@6.3.0/dist/imask.min.js" crossorigin></script>
    <script src="https://unpkg.com/croppie@2.6.5/croppie.min.js" crossorigin></script>
    <script src="https://unpkg.com/sweetalert2@11/dist/sweetalert2.all.min.js"></script>

    <script>
      flatpickr('#tanggal_lahir', {
        dateFormat: 'd-m-Y',
        enableTime: false,
        allowInput: true,
      });

      IMask($('#ponsel')[0], {
        mask: '{+628}00000000[000]',
        lazy: false,
      });
    </script>

    <script>
      try {
        var foto;

        var myCroppie = $('#foto-croppie').croppie({
          viewport: {
            width: 320,
            height: 320,
            type: 'circle',
          },
        });

        $('#foto-input').change(function() {
          myCroppie.croppie('bind', {
            url: window.URL.createObjectURL(this.files[0])
          });
          $('#foto-croppie').show();
          $('#foto-save-button').prop('disabled', false);
        });

        $('#foto-save-button').click(function() {
          myCroppie.croppie('result', {
            type: 'blob',
            circle: false,
            format: 'jpeg',
            backgroundColor: '#fff',
          }).then(function(blob) {
            foto = blob;
            $('#foto-img').attr('src', window.URL.createObjectURL(blob));
            $('#foto-modal').modal('hide');
          });
        });

        $('#profil-form').submit(function() {
          var form = this;

          Swal.fire({
            title: 'Mengirim',
            text: 'Harap tunggu',
            allowOutsideClick: function() {
              return !swal.isLoading();
            },
            didOpen: function() {
              try {
                Swal.showLoading();

                var formData = new FormData(form);

                if (foto != undefined) {
                  formData.append('foto', foto, 'foto.jpeg');
                }

                $.ajax({
                  url: $(form).attr('action'),
                  data: formData,
                  processData: false,
                  contentType: false,
                  dataType: 'json',
                  type: 'POST',
                  success: function(data) {
                    Swal.fire({
                      icon: 'success',
                      title: 'Berhasil',
                      text: data.message,
                    }).then(function() {
                      location.href = data.redirect;
                    });
                  },
                  error: function(xhr) {
                    json = JSON.parse(xhr.responseText);

                    if (json.errors != undefined) {
                      text = '';
                      $.each(json.errors, function(i, input) {
                        $.each(input, function(i, message) {
                          text += message + '<br>';
                        });
                      });
                    } else {
                      text = json.message || 'Terjadi kesalahan.';
                    }

                    Swal.fire({
                      icon: 'error',
                      title: 'Gagal',
                      html: text,
                    });
                  },
                });
              } catch (err) {
                Swal.fire({
                  icon: 'error',
                  title: 'Gagal',
                  html: err,
                });
              }
            }
          });

          return false;
        });
      } catch (err) {
        alert('Terjadi kesalahan. Coba lagi atau laporkan ini: ' + err);
      }
    </script>
  @endpush
