@extends('web.layouts.main.dashboard')

@php SEO::setTitle("Dasbor") @endphp

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Dasbor</h2>
        </div>
        <div class="card-body" style="min-height: 52vh;"></div>
      </div>
    </div>
  </div>
</div>

@endsection
