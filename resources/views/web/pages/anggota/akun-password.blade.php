@extends('web.layouts.main.dashboard')

@php SEO::setTitle("Pengaturan Akun: Password") @endphp

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header d-md-flex">
          <h2 class="h3 breadcrumb bg-transparent p-0 flex-fill my-md-auto">
            <span class="breadcrumb-item">Pengaturan Akun</span>
            <span class="breadcrumb-item text-primary">Password</span>
          </h2>

          @include('web.layouts.main.inc.nav-anggota-akun')
        </div>
        <div class="card-body d-md-flex flex-column justify-content-center" style="min-height: 52vh;">
          @messages

          <form method="post" action="{{ url()->current() }}">
            @csrf

            <div class="row">
              <div class="col-md-10 col-lg-6 mx-auto">
                @if (!admin())
                  <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">
                      Password Sekarang
                    </label>

                    <div class="col">
                      <div class="input-group">
                        <input type="password" name="current_password" class="form-control" required>
                        <div class="input-group-append">
                          <span class="input-group-text bg-secondary password-toggle"
                            style="cursor: pointer;">
                            <i class="fas fa-fw fa-eye"></i>
                            <i class="fas fa-fw fa-eye-slash d-none"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif

                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">
                    Password Baru
                  </label>

                  <div class="col">
                    <div class="input-group">
                      <input type="password" name="new_password" class="form-control" required>
                      <div class="input-group-append">
                        <span class="input-group-text bg-secondary password-toggle"
                          style="cursor: pointer;">
                          <i class="fas fa-fw fa-eye"></i>
                          <i class="fas fa-fw fa-eye-slash d-none"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">
                    Ulangi Password
                  </label>

                  <div class="col">
                    <div class="input-group">
                      <input type="password" name="new_password_confirmation" class="form-control" required>
                      <div class="input-group-append">
                        <span class="input-group-text bg-secondary password-toggle"
                          style="cursor: pointer;">
                          <i class="fas fa-fw fa-eye"></i>
                          <i class="fas fa-fw fa-eye-slash d-none"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <hr class="my-3">

            <div class="row">
              <div class="col-md-4 col-lg-3 mx-auto">
                <button type="submit" class="btn btn-primary btn-block">
                  Ubah Password
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
