@extends ('web.layouts.main.auth')

@php SEO::setTitle("Reset Password") @endphp

@section ('content')

<div class="container mt--8 mb-2">
  <div class="row mt--2 justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center mb-0">Reset Password</h1>
        </div>

        <div class="px-3 pt-3">
          @messages
        </div>

        <div class="card-body px-lg-5 pt-0 pb-0">
          <form autocomplete="on" method="post" action="{{ url()->current() }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="ni ni-email-83"></i>
                  </span>
                </div>
                <input type="email" name="email" id="email" class="form-control"
                  placeholder="Alamat email" value="{{ old('email') }}" required autofocus>
              </div>
            </div>

            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="fas fa-fw fa-key"></i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control"
                  placeholder="Password" required>
              </div>
            </div>

            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="fas fa-fw fa-key"></i>
                  </span>
                </div>
                <input type="password" name="password_confirmation" id="password_confirmation"
                  class="form-control" placeholder="Ulangi Password" required>
              </div>
            </div>

            <button type="submit" class="btn btn-block btn-primary my-4">Simpan</button>
          </form>
        </div>

        <div class="card-footer bg-transparent text-center">
          <a href="{{ route('anggota.password.forgot') }}">Kembali</a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
