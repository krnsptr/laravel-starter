@extends('web.layouts.main.dashboard')

@php SEO::setTitle("Pengaturan Akun: Email") @endphp

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header d-md-flex">
          <h2 class="h3 breadcrumb bg-transparent p-0 flex-fill my-md-auto">
            <span class="breadcrumb-item">Pengaturan Akun</span>
            <span class="breadcrumb-item text-primary">Email</span>
          </h2>

          @include('web.layouts.main.inc.nav-anggota-akun')
        </div>
        <div class="card-body d-md-flex flex-column justify-content-center" style="min-height: 52vh;">
          @messages

          @if (session('verified'))
            <div class="alert alert-success">
              Alamat email berhasil diverifikasi.
            </div>
          @endif

          @if (session('resent'))
            <div class="alert alert-success">
              <div>
                Email verifikasi berhasil dikirim.
                Jika kamu belum mendapatkannya:
              </div>

              <ul>
                <li>Cek inbox, spam, dan semua label (Sosial, Promosi, Update, dll).</li>
                <li>Pastikan inbox kamu tidak penuh dan dapat menerima pesan.</li>
                <li>Jika dalam 2-5 menit tidak masuk, silakan coba kirim ulang.</li>
              </ul>

              <div>
                <button type="button" class="btn btn-white text-success my-2"
                  id="resend-button" onclick="verify()">
                  Kirim Ulang
                  <span id="resend-countdown"></span>
                </button>
              </div>
            </div>
          @elseif (!anggota()->email_verified_at)
            <div class="alert alert-info d-md-flex">
              <div class="flex-fill my-auto">
                Alamat email kamu belum diverifikasi.
              </div>
              <button type="button" class="btn btn-white text-info my-2" onclick="verify()">
                Verifikasi Email
              </button>
            </div>
          @endif

          <form method="post" action="{{ route('anggota.email.verification.resend') }}" id="resend-form">
            @csrf
          </form>

          <form method="post" action="{{ url()->current() }}">
            @csrf

            <div class="row">
              <div class="col-md-10 col-lg-6 mx-auto">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">
                    Alamat email
                  </label>
                  <div class="col">
                    <input type="email" name="email" class="form-control"
                      value="{{ anggota()->email }}" required
                      @if (!admin() && !anggota()->canChangeEmail())
                        disabled
                      @endif>

                    @if (anggota()->email_verified_at)
                      <div class="m-1 text-success">
                        Alamat email sudah terverifikasi.
                      </div>
                    @else
                      <div class="m-1 text-danger">
                        Alamat email belum terverifikasi.
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>

            @if (admin() || anggota()->canChangeEmail())
              <hr class="my-3">

              <div class="row">
                <div class="col-md-4 col-lg-3 mx-auto">
                  <button type="submit" class="btn btn-primary btn-block">Ubah</button>
                </div>
              </div>
            @endif
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('js')
  <script src="https://unpkg.com/sweetalert2@11/dist/sweetalert2.all.min.js"></script>
  <script src="https://unpkg.com/jquery-countdown@2.2.0/dist/jquery.countdown.min.js"></script>
  <script src="https://unpkg.com/dayjs@1.10.7/dayjs.min.js"></script>
  <script>
    function verify() {
      email = @json(anggota()->email);

      Swal.fire({
        icon: 'question',
        title: 'Verifikasi Email',
        html: 'Pastikan alamat email aktif dan dapat menerima pesan.' +
          ' Apakah alamat email ini sudah benar?' +
          '<br><strong class="text-monospace">' + email + '</strong>',
        showCancelButton: true,
        confirmButtonText: 'Ya, kirim email',
        cancelButtonText: 'Tidak, ubah email'
      }).then(function(result) {
        if (result.isConfirmed) {
          $('#resend-form').submit();
        }
      });
    }

    if ($('#resend-button').length) {
      $(document).ready(function () {
        $('#resend-button').prop('disabled', true);

        $('#resend-countdown').countdown(dayjs().add('2', 'minute').toDate(), function (event) {
          $(this).html(event.strftime('(%M:%S)'));
        }).on('finish.countdown', function() {
          $(this).html('');
          $('#resend-button').prop('disabled', false);
        });
      });
    }
  </script>
@endpush
