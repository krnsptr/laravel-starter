@extends ('web.layouts.main.auth')

@php SEO::setTitle("Admin | Masuk") @endphp

@section ('content')

<div class="container mt--8 mb--1">
  <div class="row mt--2 justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center mb-0">Masuk sebagai Admin</h1>
        </div>

        <div class="px-3 pt-3">
          @messages
        </div>

        <div class="card-body px-lg-5 pt-0 pb-0">
          <form autocomplete="on" method="post" action="{{ url()->current() }}">
            @csrf

            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="far fa-fw fa-envelope"></i>
                  </span>
                </div>
                <input type="email" name="email" id="email" class="form-control"
                  placeholder="Alamat email" value="{{ old('email') }}" required autofocus>
              </div>
            </div>

            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="fas fa-fw fa-key"></i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control"
                  placeholder="Kata Sandi" required>
              </div>
            </div>

            <div class="custom-control custom-control-alternative custom-checkbox">
              <input type="checkbox" name="remember" id="remember" class="custom-control-input">
              <label class="custom-control-label" for="remember">
                <span class="text-muted">Ingat saya</span>
              </label>
            </div>

            <button type="submit" class="btn btn-block btn-primary my-4">
              Masuk
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
