@extends('web.layouts.main.landing')

@section('content')

<div class="container mt--8 mb--1">
  <div class="row text-center">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h1 class="mb-0">
            {{ config('app.name') }}
          </h1>
        </div>
        <div class="card-body" style="min-height: 52vh;">
          <div class="row">
            <div class="col-lg-4 mx-auto my-3">
              <a href="{{ route('anggota.dasbor') }}" class="card border shadow-sm">
                <div class="card-body">
                  <i class="fa fa-user fa-5x text-primary"></i>
                  <div class="h4 mt-3">
                    Anggota
                  </div>
                </div>
              </a>
            </div>

            <div class="col-lg-4 mx-auto my-3">
              <a href="{{ route('admin.dasbor') }}" class="card border shadow-sm">
                <div class="card-body">
                  <i class="fa fa-user-cog fa-5x text-info"></i>
                  <div class="h4 mt-3">
                    Admin
                  </div>
                </div>
              </a>
            </div>

            <div class="col-lg-4 mx-auto my-3">
              <a href="{{ route('developer.home') }}" class="card border shadow-sm">
                <div class="card-body">
                  <i class="fa fa-user-secret fa-5x text-default"></i>
                  <div class="h4 mt-3">
                    Developer
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
