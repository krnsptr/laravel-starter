<?php

namespace App\Traits;

use Illuminate\Auth\MustVerifyEmail;

trait HasEmailVerification
{
    use MustVerifyEmail;

    public static function isEmailVerificationEnabled(): bool
    {
        return (bool) config('auth.email.verification.enabled');
    }
}
