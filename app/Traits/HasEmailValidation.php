<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * @property string|null $email
 * @property \Carbon\CarbonInterface|null $email_validated_at
 */
trait HasEmailValidation
{
    public $skipEmailValidation = false;

    public static function bootHasEmailValidation()
    {
        static::saving(function (Model $model) {
            /** @var self|Model $model */
            $model->validateEmail();
        });
    }

    public static function isEmailValidationEnabled(): bool
    {
        return (bool) config('auth.email.validation.enabled');
    }

    public function shouldValidateEmail(): bool
    {
        return static::isEmailValidationEnabled()
            && $this->isDirty('email')
            && !empty($this->email)
            && empty($this->skipEmailValidation);
    }

    public function validateEmail(): void
    {
        if (!$this->shouldValidateEmail()) {
            if ($this->isDirty('email')) {
                $this->email_validated_at = null;
            }

            return;
        }

        Validator::make(['email' => $this->email], [
            'email' => function ($attribute, $value, $fail) {
                $email = $this->email;
                $verifier = new \hbattat\VerifyEmail($email, \config('mail.from.address') ?: $email);

                if (!$verifier->verify()) {
                    $fail("Alamat email tidak valid: $email");
                } else {
                    $this->email_validated_at = now();
                }
            },
        ])->validate();
    }

    public function saveWithoutEmailValidation()
    {
        $this->skipEmailValidation = true;
        $this->save();
    }
}
