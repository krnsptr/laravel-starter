<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @property string|null $foto
 */
trait HasFoto
{
    public ?string $foto_old = null;

    public static function bootHasFoto()
    {
        static::saving(function (Model $model) {
            $model->foto_old = $model->getOriginal('foto');
        });

        static::saved(function (Model $model) {
            /** @var self|Model $model */
            $model->deleteOldFoto();
            $model->foto_old = null;
        });

        static::deleted(function (Model $model) {
            $model->deleteFoto();
            $model->foto_old = null;
        });
    }

    public static function fotoDisk(): string
    {
        return 'public';
    }

    public static function fotoStorage(): FilesystemAdapter
    {
        return Storage::disk(static::fotoDisk());
    }

    public static function fotoDefault(): string
    {
        return '_default.png';
    }

    public function fotoDir(): string
    {
        $tableName = (string) $this->getTable();

        return Str::slug($tableName) . '/' . 'foto';
    }

    public function fotoFileName(): string
    {
        return $this->foto ?: static::fotoDefault();
    }

    public function fotoPath(): string
    {
        return $this->fotoDir() . '/' . $this->fotoFileName();
    }

    public function fotoOldPath(): string
    {
        return $this->fotoDir() . '/' . $this->foto_old;
    }

    public function fotoUrl(): string
    {
        return static::fotoStorage()->url($this->fotoPath());
    }

    public function shouldDeleteFoto($filename): bool
    {
        return !empty($filename)
            && $filename !== static::fotoDefault()
            && !\Str::of($filename)->startsWith('_');
    }

    public function deleteFoto(): void
    {
        if ($this->shouldDeleteFoto($this->foto)) {
            static::fotoStorage()->delete($this->fotoPath());
        }
    }

    public function deleteOldFoto(): void
    {
        if (
            $this->shouldDeleteFoto($this->foto_old)
            && $this->foto_old !== $this->foto
        ) {
            static::fotoStorage()->delete($this->fotoOldPath());
        }
    }

    protected function generateFotoFilename(): string
    {
        $id = (int) $this->id;
        $uuid = (string) Str::orderedUuid();

        return "$id--$uuid";
    }

    public function setFoto(UploadedFile $uploaded_file): void
    {
        $extension = $uploaded_file->extension();
        $filename = $this->generateFotoFilename();
        $filename .= ".$extension";

        $this->fotoStorage()->putFileAs($this->fotoDir(), $uploaded_file, $filename);
        $this->foto = $filename;
    }

    public function getFotoUrlAttribute(): string
    {
        return $this->fotoUrl();
    }
}
