<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Spatie\RouteAttributes\Attributes\Get;

class DeveloperController extends WebController
{
    public function __construct()
    {
        $this->middleware('auth.dev');
    }

    #[Get('/dev', 'developer.home')]
    public function home()
    {
        return view('web.pages.developer.home');
    }

    #[Get('/dev/phpinfo', 'developer.phpinfo')]
    public function phpinfo()
    {
        return view('web.pages.developer.phpinfo');
    }
}
