<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Spatie\RouteAttributes\Attributes\Get;

class HomeController extends WebController
{
    #[Get('/', 'home')]
    public function home()
    {
        return view('web.pages.home');
    }
}
