<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Route;

class AnggotaController extends WebController
{
    public function __construct()
    {
        $this->middleware('auth:anggota');
        $this->middleware('email.check')->except('akun', 'akun_email');
        $this->middleware('anggota.profil.check')->except('akun', 'akun_email', 'profil');
    }

    #[Get('/dasbor', 'anggota.dasbor')]
    public function dasbor()
    {
        return view('web.pages.anggota.dasbor');
    }

    #[Get('/akun', 'anggota.akun')]
    public function akun()
    {
        return redirect()->route('anggota.akun.email');
    }

    #[Route(['get', 'post'], '/akun/email', 'anggota.akun.email')]
    public function akun_email(Request $request)
    {
        $anggota = anggota();

        if ($request->isMethod('post')) {
            if (!admin() && !$anggota->canChangeEmail()) {
                return redirect()->back()->withErrors([
                    'email' => 'Alamat email tidak dapat diubah.',
                ]);
            }

            $anggota->email = $request->input('email');
            $anggota->save();

            return redirect()->back()->withSuccess('Alamat email berhasil diubah.');
        }

        return view('web.pages.anggota.akun-email');
    }

    #[Route(['get', 'post'], '/akun/password', 'anggota.akun.password')]
    public function akun_password(Request $request)
    {
        $anggota = anggota();

        if ($request->isMethod('post')) {
            $this->validate(
                request: $request,
                rules: [
                    'current_password' => admin() ? '' : 'required|string|current_password:anggota',
                    'new_password' => 'required|string|min:8|confirmed',
                ],
                customAttributes: [
                    'current_password' => 'Password Sekarang',
                    'new_password' => 'Password Baru',
                ],
            );

            $anggota->password = bcrypt($request->input('new_password'));
            $anggota->save();

            return redirect()->back()->withSuccess('Password berhasil diubah.');
        }

        return view('web.pages.anggota.akun-password');
    }

    #[Route(['get', 'post'], '/profil', 'anggota.profil')]
    public function profil(Request $request)
    {
        $anggota = anggota();

        $profil = $anggota->profil ?? $anggota->profil()->make();

        if ($request->isMethod('post')) {
            $anggota->fill($request->only('nama'));
            $profil->fill($request->all());

            \DB::beginTransaction();

            if ($profil->isClean()) {
                $profil->touch();
            }

            $anggota->save();
            $profil->save();

            if ($request->hasFile('foto')) {
                $anggota->setFoto($request->file('foto'));
                $anggota->save();
            }

            \DB::commit();

            $redirect = route('anggota.profil');
            $message = 'Profil berhasil disimpan.';

            if ($request->wantsJson()) {
                return \response()->json([
                    'message' => $message,
                    'redirect' => $redirect,
                ]);
            }

            return \redirect($redirect)->withSuccess($message);
        }

        return view('web.pages.anggota.profil', compact(
            'anggota',
            'profil',
        ));
    }
}
