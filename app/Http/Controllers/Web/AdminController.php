<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Spatie\RouteAttributes\Attributes\Get;

class AdminController extends WebController
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    #[Get('/admin', 'admin.dasbor')]
    public function dasbor()
    {
        return view('web.pages.admin.dasbor');
    }
}
