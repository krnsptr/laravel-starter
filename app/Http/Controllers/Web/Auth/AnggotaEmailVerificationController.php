<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\WebController;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;

class AnggotaEmailVerificationController extends WebController
{
    use VerifiesEmails;

    public function __construct()
    {
        $this->middleware('auth:anggota');
        $this->middleware('throttle:email')->only('doResend');
    }

    /**
     * Where to redirect users after email verified.
     *
     * @return string
     */
    public function redirectTo()
    {
        return route('anggota.akun.email');
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        $request_email = (string) $request->input('email');
        $request_hash = (string) $request->input('hash');
        $anggota_email = (string) $request->user()->getEmailForVerification();

        if (!$request->hasValidSignature()) {
            return redirect()
                ->route('anggota.akun.email')
                ->withErrors([
                    'signature' => 'Tautan telah kedaluwarsa. Silakan coba lagi.',
                ]);
        }

        if (
            $request_email !== $anggota_email
            || !hash_equals($request_hash, sha1($anggota_email))
        ) {
            if ($request->wantsJson()) {
                throw new AuthorizationException();
            }

            return redirect()
                ->route('anggota.akun.email')
                ->withErrors([
                    'email' => "
                        Email yang diverifikasi ($request_email)
                        tidak cocok dengan akun yang sedang login ($anggota_email).
                    ",
                ]);
        }

        if ($request->user()->hasVerifiedEmail()) {
            return $request->wantsJson()
                ? new JsonResponse([], 204)
                : redirect($this->redirectPath());
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        if ($response = $this->verified($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect($this->redirectPath())->with('verified', true);
    }

    #[Post('/email/verifikasi/kirim-ulang', 'anggota.email.verification.resend')]
    public function doResend(Request $request)
    {
        return $this->resend($request);
    }

    #[Get('/email/verifikasi', 'anggota.email.verification.verify')]
    public function doVerify(Request $request)
    {
        return $this->verify($request);
    }
}
