<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\WebController;
use App\Models\Anggota;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;

class AnggotaRegisterController extends WebController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('anggota');
    }

    /**
     * Where to redirect users after registration.
     *
     * @return string
     */
    public function redirectTo()
    {
        return route('anggota.profil');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('web.pages.anggota.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string|min:8',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $email = $request->input('email');

        if (Anggota::where('email', $email)->exists()) {
            return redirect()->route('anggota.login')->withErrors(
                ['email' => "Alamat email $email sudah terdaftar."],
            );
        }

        $anggota = $this->create($request->all());

        event(new Registered($anggota));

        $this->guard()->login($anggota);

        $request->session()->flash('success', 'Pendaftaran berhasil.');

        return $this->registered($request, $anggota) ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Anggota
     */
    protected function create(array $data)
    {
        $anggota = new Anggota();
        $anggota->nama = $data['nama'];
        $anggota->email = $data['email'];
        $anggota->password = bcrypt($data['password']);
        $anggota->save();

        return $anggota;
    }

    #[Get('/daftar', 'anggota.register')]
    public function showForm()
    {
        return $this->showRegistrationForm();
    }

    #[Post('/daftar', 'anggota.register')]
    public function doRegister(Request $request)
    {
        return $this->register($request);
    }
}
