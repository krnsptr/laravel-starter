<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\WebController;
use App\Models\Anggota;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;

class AnggotaResetPasswordController extends WebController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('anggota');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('anggota');
    }

    /**
     * Where to redirect users after resetting their password.
     *
     * @return string
     */
    public function redirectTo()
    {
        return route('anggota.login');
    }

    /**
     * Reset the given user's password.
     *
     * @param  Anggota  $anggota
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($anggota, $password)
    {
        $anggota->password = bcrypt($password);

        $anggota->setRememberToken(Str::random(60));

        $anggota->save();

        event(new PasswordReset($anggota));
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return view('web.pages.anggota.password-reset')->with(
            ['token' => $token, 'email' => $request->email],
        );
    }

    #[Get('/password/reset', 'anggota.password.reset')]
    public function showForm(Request $request)
    {
        $token = $request->input('token');

        return $this->showResetForm($request, $token);
    }

    #[Post('/password/reset', 'anggota.password.reset')]
    public function doReset(Request $request)
    {
        return $this->reset($request);
    }
}
