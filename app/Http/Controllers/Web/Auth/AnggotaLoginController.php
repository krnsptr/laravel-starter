<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\WebController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;

class AnggotaLoginController extends WebController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('doLogout', 'showLogoutForm');
        $this->middleware('auth:anggota')->only('doLogout', 'showLogoutForm');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('anggota');
    }

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectTo()
    {
        return route('anggota.dasbor');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        if ($request->user('admin')) {
            return redirect()->route('admin.dasbor');
        }

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    #[Get('/masuk', 'anggota.login')]
    public function showLoginForm()
    {
        return view('web.pages.anggota.login');
    }

    #[Get('/keluar', 'anggota.logout')]
    public function showLogoutForm()
    {
        return view('web.pages.anggota.logout');
    }

    #[Post('/masuk', 'anggota.login')]
    public function doLogin(Request $request)
    {
        return $this->login($request);
    }

    #[Post('/keluar', 'anggota.logout')]
    public function doLogout(Request $request)
    {
        return $this->logout($request);
    }
}
