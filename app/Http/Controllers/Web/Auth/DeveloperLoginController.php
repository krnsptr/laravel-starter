<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\WebController;
use App\Http\Middleware\DeveloperFeatureCheck;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;

class DeveloperLoginController extends WebController
{
    use AuthenticatesUsers;

    protected Authenticatable $anggota;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(DeveloperFeatureCheck::class);
        $this->middleware('guest:developer')->except('doLogout', 'showLogoutForm');
        $this->middleware('auth:developer')->only('doLogout', 'showLogoutForm');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Mpyw\NullAuth\NullGuard
     */
    protected function guard()
    {
        return Auth::guard('developer');
    }

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectTo()
    {
        return \route('developer.home');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    #[Get('/dev/login', 'developer.login')]
    public function showLoginForm()
    {
        return \view('web.pages.developer.login');
    }

    #[Get('/dev/logout', 'developer.logout')]
    public function showLogoutForm()
    {
        return \view('web.pages.developer.logout');
    }

    #[Post('/dev/login', 'developer.login')]
    public function doLogin(Request $request)
    {
        return $this->login($request);
    }

    #[Post('/dev/logout', 'developer.logout')]
    public function doLogout(Request $request)
    {
        return $this->logout($request);
    }
}
