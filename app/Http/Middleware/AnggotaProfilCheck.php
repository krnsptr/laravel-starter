<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AnggotaProfilCheck
{
    public function handle(Request $request, Closure $next)
    {
        $anggota = anggota();
        $profil = $anggota->profil ?? null;

        if (!isset($profil) || !$profil->isComplete()) {
            return $this->abort('Ayo lengkapi profilmu!');
        }

        if (!$profil->isUpToDate()) {
            return $this->abort('Pastikan profilmu lengkap dan up-to-date. Jika sudah, klik Simpan.');
        }

        return $next($request);
    }

    protected function abort(string $error_message)
    {
        session()->reflash();

        if (request()->expectsJson()) {
            return \abort(403, $error_message);
        }

        return \redirect()->route('anggota.profil')->withWarning($error_message);
    }
}
