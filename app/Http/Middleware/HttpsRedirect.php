<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HttpsRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (
            !$request->secure()
            && !app()->environment('local')
            && !in_array($request->server('REMOTE_ADDR'), ['127.0.0.1', '::1'])
        ) {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}
