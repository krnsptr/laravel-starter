<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\RobotsMiddleware\RobotsMiddleware as BaseMiddleware;

class RobotsMiddleware extends BaseMiddleware
{
    protected function shouldIndex(Request $request)
    {
        return !Route::isProtected();
    }
}
