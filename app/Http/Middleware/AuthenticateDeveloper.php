<?php

namespace App\Http\Middleware;

use App\Http\Middleware\Authenticate;
use App\Http\Middleware\DeveloperFeatureCheck;
use Closure;
use Illuminate\Http\Request;

class AuthenticateDeveloper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return (new DeveloperFeatureCheck())->handle($request, function () use ($request, $next) {
            return (new Authenticate(app('auth')))->handle($request, $next, 'developer');
        });
    }
}
