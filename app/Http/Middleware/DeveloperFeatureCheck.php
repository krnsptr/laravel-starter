<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DeveloperFeatureCheck
{
    public function handle(Request $request, Closure $next)
    {
        if (!config('auth.providers.developer.enabled')) {
            abort(404);
        }

        return $next($request);
    }
}
