<?php

namespace App\Http\Middleware;

use App\Contracts\HasEmailVerificationContract;
use Closure;
use Illuminate\Http\Request;

class EmailCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $anggota = $request->user();

        if (
            $anggota instanceof HasEmailVerificationContract
            && config('auth.email.verification.enabled')
            && !$anggota->email_verified_at
        ) {
            $message = 'Ayo verifikasi alamat email kamu!';

            if ($request->expectsJson()) {
                return \abort(403, $message);
            }

            $request->session()->reflash();

            return \redirect()->route('anggota.akun.email')->withWarning($message);
        }

        return $next($request);
    }
}
