<?php

namespace App\Notifications;

use App\Models\Anggota;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;

class AnggotaVerifyEmailNotification extends VerifyEmail
{
    public Anggota $anggota;

    public int $expiryMinutes;

    public string $verificationUrl;

    public function __construct(Anggota $anggota)
    {
        $this->anggota = $anggota;

        $this->setExpiryMinutes();
        $this->setVerificationUrl();
    }

    protected function setExpiryMinutes(): void
    {
        $this->expiryMinutes = (int) config('auth.email.verification.expire') ?: 60;
    }

    protected function setVerificationUrl(): void
    {
        $this->verificationUrl = URL::temporarySignedRoute(
            'anggota.email.verification.verify',
            now()->addMinutes($this->expiryMinutes),
            [
                'email' => $this->anggota->getEmailForVerification(),
                'hash' => sha1($this->anggota->getEmailForVerification()),
            ],
        );
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Verifikasi Email')
            ->greeting("Halo, {$notifiable->nama}!")
            ->line('Silakan klik tombol berikut untuk melakukan verifikasi email.')
            ->action('Verifikasi Email', $this->verificationUrl)
            ->line("Tautan ini akan kedaluwarsa dalam {$this->expiryMinutes} menit.");
    }
}
