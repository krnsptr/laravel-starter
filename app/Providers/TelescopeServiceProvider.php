<?php

namespace App\Providers;

use App\Models\Developer;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\IncomingExceptionEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Telescope::night();

        $this->hideSensitiveRequestDetails();

        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->environment('local')) {
                return true;
            }

            return $entry->isReportableException()
                   || $entry->isFailedRequest()
                   || $entry->isFailedJob()
                   || $entry->isScheduledTask()
                   || $entry->hasMonitoredTag();
        });

        if (config('logging.channels.slack.url')) {
            Telescope::afterStoring(function (array $entries) {
                foreach ($entries as $entry) {
                    if ($entry instanceof IncomingExceptionEntry) {
                        if (app()->environment('local')) {
                            $rate_limiter_attempts = 5;
                            $rate_limiter_seconds = 60;
                        } else {
                            $rate_limiter_attempts = 1;
                            $rate_limiter_seconds = 30 * 60;
                        }

                        $rate_limiter_key = 'logging.channels.slack'
                            . '.'
                            . Str::slug($entry->exception->getFile())
                            . '#'
                            . $entry->exception->getLine();

                        RateLimiter::attempt(
                            $rate_limiter_key,
                            $rate_limiter_attempts,
                            function () use ($entry) {
                                logger()->channel('slack')->critical(
                                    $entry->exception,
                                    [
                                        'environment' => app()->environment(),
                                        'url' => app()->runningInConsole()
                                            ? 'CLI'
                                            : request()->method() . ' ' . request()->fullUrl(),
                                        'user' => $entry->content['user'] ?? '-',
                                        'view in Telescope' => url(
                                            config('telescope.path')
                                                . '/exceptions'
                                                . '/'
                                                . $entry->uuid,
                                        ),
                                    ],
                                );
                            },
                            $rate_limiter_seconds,
                        );
                    }
                }
            });
        }
    }

    /**
     * Prevent sensitive request details from being logged by Telescope.
     *
     * @return void
     */
    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }

        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    /**
     * Register the Telescope gate.
     *
     * This gate determines who can access Telescope in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewTelescope', function ($anggota) {
            \Log::debug(json_encode($anggota));

            return $anggota instanceof Developer;
        });
    }
}
