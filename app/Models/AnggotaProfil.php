<?php

namespace App\Models;

use App\BaseModel;
use App\Enums\JenisKelamin;
use App\Models\Anggota;
use App\Rules\ExistsEloquent;
use Deiucanta\Smart\Field;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;

class AnggotaProfil extends BaseModel
{
    use HasFactory;

    public const REGEX_PONSEL = '/^\+628\d{2}\d{4}\d{2,5}$/';

    protected $table = 'anggota_profil';

    public $modelLabel = 'Profil';

    public function fields()
    {
        return [
            Field::make('id')->unsignedBigInteger()->rule(new ExistsEloquent(Anggota::class)),
            Field::make('jenis_kelamin')->required()->fillable()->char(1)
                ->label('Jenis kelamin')
                ->rule(Rule::in(JenisKelamin::getValues())),
            Field::make('tanggal_lahir')->required()->fillable()->date()
                ->label('Tanggal lahir')
                ->rule('date|before:now'),
            Field::make('ponsel')->required()->fillable()->string()
                ->label('Nomor ponsel')
                ->rule('regex:' . self::REGEX_PONSEL),
            Field::make('created_at')->nullable()->timestamp(),
            Field::make('updated_at')->nullable()->timestamp(),
        ];
    }

    public function getJenisKelaminEnumAttribute(): ?JenisKelamin
    {
        return JenisKelamin::coerce($this->jenis_kelamin);
    }

    public function getJenisKelaminTextAttribute(): ?string
    {
        return $this->jenis_kelamin_enum->description ?? null;
    }

    public function isComplete(): bool
    {
        $instance = new static();

        /** @var \Deiucanta\Smart\Field[] $required_fields */
        $required_fields = collect($instance->fields())
            ->filter(function ($field) {
                return !$field->nullable;
            });

        $rules = [];

        foreach ($required_fields as $field) {
            $rules[$field->name] = ['required'];
        }

        return \Validator::make(
            data: $this->getData(),
            rules: $rules,
        )->passes();
    }

    public function isUpToDate(): bool
    {
        return $this->updated_at >= now()->subMonth(6);
    }
}
