<?php

namespace App\Models;

use App\BaseUser;
use App\Traits\HasFoto;
use Deiucanta\Smart\Field;

class Admin extends BaseUser
{
    use HasFoto;

    protected $table = 'admin';

    public $modelLabel = 'admin';

    public function fields()
    {
        return [
            Field::make('id')->bigIncrements(),
            Field::make('nama')->required()->fillable()->string()->label('Nama'),
            Field::make('email')->required()->fillable()->email()->unique()->label('Email'),
            Field::make('password')->required()->hidden()->string()->label('Password'),
            Field::make('foto')->nullable()->string()->label('Foto profil'),
            Field::make('remember_token')->nullable()->hidden()->string(),
            Field::make('created_at')->nullable()->timestamp(),
            Field::make('updated_at')->nullable()->timestamp(),
        ];
    }
}
