<?php

namespace App\Models;

use App\BaseUser;
use App\Contracts\HasEmailVerificationContract;
use App\Models\AnggotaProfil;
use App\Notifications\AnggotaForgotPasswordNotification;
use App\Notifications\AnggotaVerifyEmailNotification;
use App\Traits\HasEmailValidation;
use App\Traits\HasEmailVerification;
use App\Traits\HasFoto;
use Deiucanta\Smart\Field;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;

class Anggota extends BaseUser implements HasEmailVerificationContract
{
    use HasFactory;
    use HasEmailValidation;
    use HasEmailVerification;
    use HasFoto;
    use Notifiable;

    protected $table = 'anggota';

    public $modelLabel = 'pengguna';

    public function fields()
    {
        return [
            Field::make('id')->bigIncrements(),
            Field::make('nama')->required()->fillable()->string()->label('Nama'),
            Field::make('email')->required()->fillable()->email()->unique()->label('Email'),
            Field::make('password')->required()->hidden()->string()->label('Password'),
            Field::make('foto')->nullable()->string()->label('Foto profil'),
            Field::make('remember_token')->nullable()->hidden()->string(),
            Field::make('email_validated_at')->nullable()->timestamp(),
            Field::make('email_verified_at')->nullable()->timestamp(),
            Field::make('created_at')->nullable()->timestamp(),
            Field::make('updated_at')->nullable()->timestamp(),
        ];
    }

    public function profil()
    {
        return $this->hasOne(AnggotaProfil::class, 'id');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AnggotaForgotPasswordNotification($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new AnggotaVerifyEmailNotification($this));
    }

    public function canChangeEmail(): bool
    {
        return !$this->email_validated_at && !$this->email_verified_at;
    }
}
