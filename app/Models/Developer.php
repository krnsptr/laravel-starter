<?php

namespace App\Models;

use App\BaseUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Sushi\Sushi;

class Developer extends BaseUser
{
    use Sushi;

    public function getRows()
    {
        $username = config('auth.providers.developer.username');
        $password = config('auth.providers.developer.password');

        if (!Str::startsWith($password, '$')) {
            $password = Hash::make($password);
        }

        return [compact('username', 'password')];
    }

    public function getNameAttribute(): ?string
    {
        return $this->username;
    }

    public function getEmailAttribute(): ?string
    {
        return $this->username;
    }
}
