<?php

namespace App\Rules;

use Korridor\LaravelModelValidationRules\Rules\UniqueEloquent as BaseUniqueEloquent;

class UniqueEloquent extends BaseUniqueEloquent
{
    public function message(): string
    {
        return ":attribute sudah terdaftar.";
    }
}
