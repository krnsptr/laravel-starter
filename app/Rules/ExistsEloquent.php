<?php

namespace App\Rules;

use Korridor\LaravelModelValidationRules\Rules\ExistsEloquent as BaseExistsEloquent;

class ExistsEloquent extends BaseExistsEloquent
{
    public function message(): string
    {
        return ":attribute tidak terdaftar";
    }
}
