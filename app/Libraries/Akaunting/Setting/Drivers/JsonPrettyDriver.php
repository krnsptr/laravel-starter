<?php

namespace App\Libraries\Akaunting\Setting\Drivers;

use Akaunting\Setting\Drivers\Json;

class JsonPrettyDriver extends Json
{
    protected function write(array $data)
    {
        if ($data) {
            $contents = json_encode($data, JSON_PRETTY_PRINT);
        } else {
            $contents = '{}';
        }

        $this->files->put($this->path, $contents);
    }
}
