<?php

namespace App\Enums;

use Krnsptr\LaravelEnum\LaravelEnum;

/**
 * Binary gender enum using ISO 5218
 *
 * @method static static LakiLaki()
 * @method static static Perempuan()
 */
final class JenisKelamin extends LaravelEnum
{
    public const LakiLaki = 'L';

    public const Perempuan = 'P';
}
