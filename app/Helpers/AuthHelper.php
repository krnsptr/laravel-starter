<?php

use App\Models\Admin;
use App\Models\Anggota;
use App\Models\Developer;

function developer(): ?Developer
{
    return auth('developer')->user();
}

function admin(): ?Admin
{
    return auth('admin')->user();
}

function anggota(): ?Anggota
{
    return auth('anggota')->user();
}
