<?php

use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;

function format_integer($number): ?string
{
    if ($number === null) {
        return null;
    }

    return number_format($number, 0, ',', '.');
}

function format_rupiah($number): ?string
{
    if ($number === null) {
        return null;
    }

    return 'Rp' . format_integer($number);
}

function format_date_php($date, $format = 'd-m-Y'): ?string
{
    if ($date === null) {
        return null;
    }

    if ($date instanceof CarbonInterface) {
        return $date->format($format);
    }

    return CarbonImmutable::parse($date)->format($format);
}

function format_date_moment($date, $format = 'LL'): ?string
{
    if ($date === null) {
        return null;
    }

    if ($date instanceof CarbonInterface) {
        return $date->isoFormat($format);
    }

    return CarbonImmutable::parse($date)->isoFormat($format);
}

function format_date($date): ?string
{
    return format_date_moment($date, 'LL');
}

function format_datetime($date): ?string
{
    return format_date_moment($date, 'LLL');
}
