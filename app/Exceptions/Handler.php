<?php

namespace App\Exceptions;

use App;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\VarDumper\VarDumper;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $e
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $e)
    {
        if ($e instanceof ValidationException) {
            if (App::runningInConsole() && !App::runningUnitTests()) {
                /** @var \Illuminate\Validation\Validator $validator */
                $validator = $e->validator;

                $errors = $e->errors();
                $data = $validator->getData();

                $invalid_data = array_intersect_key($data, $errors);

                (new VarDumper())->dump($errors);
                (new VarDumper())->dump($invalid_data);
            }
        }

        parent::report($e);
    }
}
