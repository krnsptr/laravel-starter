<?php

namespace App\Contracts;

use Illuminate\Contracts\Auth\MustVerifyEmail;

interface HasEmailVerificationContract extends MustVerifyEmail
{
    public static function isEmailVerificationEnabled(): bool;
}
