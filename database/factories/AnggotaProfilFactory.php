<?php

namespace Database\Factories;

use App\Enums\JenisKelamin;
use App\Models\AnggotaProfil;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnggotaProfilFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        return [
            'jenis_kelamin' => JenisKelamin::getRandomValue(),
            'tanggal_lahir' => $faker->dateTimeBetween('-18 year', 'now'),
            'ponsel' => $faker->regexify(AnggotaProfil::REGEX_PONSEL),
        ];
    }
}
