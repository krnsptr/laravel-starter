<?php

namespace Database\Factories;

use App\Models\Anggota;
use App\Models\AnggotaProfil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AnggotaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Anggota::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'email_validated_at' => now(),
            'email_verified_at' => now(),
        ];
    }

    /**
     * Indicate that the model's email address should be unvalidated.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unvalidated()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_validated_at' => null,
            ];
        });
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Anggota $anggota) {
            AnggotaProfil::factory()->create([
                'id' => $anggota->id,
            ]);
        });
    }
}
