<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota_profil', function (Blueprint $table) {
            $table->id();
            $table->char('jenis_kelamin', 1);
            $table->date('tanggal_lahir');
            $table->string('ponsel');
            $table->timestamps();

            $table->foreign('id')->references('id')->on('anggota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota_profil');
    }
};
