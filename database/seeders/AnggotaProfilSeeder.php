<?php

namespace Database\Seeders;

use App\Models\Anggota;
use App\Models\AnggotaProfil;
use Illuminate\Database\Seeder;

class AnggotaProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        for ($i = 1; $i <= Anggota::count(); $i++) {
            AnggotaProfil::factory()->create(['id' => $i]);
        }

        \DB::commit();
    }
}
