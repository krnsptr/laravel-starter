<?php

namespace Database\Seeders;

use App\Models\Anggota;
use Illuminate\Database\Seeder;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        for ($i = 1; $i <= 9; $i++) {
            Anggota::factory()->create([
                'nama' => "User $i",
                'email' => $email = "user$i@example.com",
                'password' => bcrypt($email),
                'email_validated_at' => now(),
                'email_verified_at' => now(),
            ]);
        }

        Anggota::factory(91)->create();

        \DB::commit();
    }
}
