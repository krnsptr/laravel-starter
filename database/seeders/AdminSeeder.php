<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        Admin::create([
            'nama' => 'Administrator',
            'email' => $email = 'admin@example.com',
            'password' => bcrypt($email),
        ]);

        \DB::commit();
    }
}
