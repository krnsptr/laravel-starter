<?php

namespace Database\Seeders;

use App;
use Database\Seeders\AdminSeeder;
use Database\Seeders\AnggotaSeeder;
use Database\Seeders\SettingSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingSeeder::class);

        $this->call(AdminSeeder::class);

        if (App::environment(['local', 'testing'])) {
            $this->call(AnggotaSeeder::class);
        }
    }
}
